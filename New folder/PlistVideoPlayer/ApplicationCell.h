#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ApplicationCell : UITableViewCell


@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic,strong) IBOutlet UILabel *pubDateLabel;
@property (nonatomic,strong) IBOutlet UIImageView *icon;

@property BOOL useCellBackground;


@end