//
//  ApplicationCell2.h
//  PlistVideoPlayer
//
//  Created by Istvan Szabo on 2012.01.03..
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ApplicationCell2 : UITableViewCell


@property (nonatomic,strong) IBOutlet UILabel *titleLabel;


@property BOOL useCellBackground;


@end