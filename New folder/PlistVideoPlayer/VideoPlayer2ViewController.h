#import <UIKit/UIKit.h>
#import "ApplicationCell2.h"
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoPlayer2ViewController : UITableViewController

@property (nonatomic, strong) NSArray *videoPlist;
@property (nonatomic, strong) NSDictionary *releasePlist;

@property (nonatomic,strong) IBOutlet UILabel *videoTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *videoDescriptionLabel;
@property (nonatomic,strong) IBOutlet UILabel *categoryLabel;
@property (nonatomic,strong) IBOutlet UILabel *copirightLabel;
@property (nonatomic,strong) IBOutlet UIImageView *headerIcon;
@property (nonatomic,strong) MPMoviePlayerViewController *theMoviePlayer;
- (IBAction)refresh_:(id)sender;

- (void)loadVideoPlayer:(NSString *)string;

@end
