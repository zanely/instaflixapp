#import <UIKit/UIKit.h>
#import "ApplicationCell.h"


@interface VideoPlayerViewController : UITableViewController

@property (nonatomic, strong) NSArray *videoPlist;
@property (nonatomic, strong) NSDictionary *releasePlist;

- (IBAction)refresh_:(id)sender;

- (void)loadVideoPlayer:(NSString *)string;

@end
