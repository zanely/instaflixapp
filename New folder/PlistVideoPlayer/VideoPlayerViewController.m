#import "VideoPlayerViewController.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import <MediaPlayer/MediaPlayer.h>


// Customized
#define URL_DATA @"http://cdn.instanetwork.nl/Videos.plist"
#define CELL_BACKGROUND_1 [UIColor colorWithRed:85.0/255.0 green:74.0/255.0 blue:65.0/255.0 alpha:1.0]
#define CELL_BACKGROUND_2 [UIColor colorWithRed:98.0/255.0 green:87.0/255.0 blue:77.0/255.0 alpha:1.0]


@implementation VideoPlayerViewController

@synthesize videoPlist;
@synthesize releasePlist;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.releasePlist = [NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:URL_DATA]];
    self.videoPlist = [releasePlist objectForKey:@"Videos"];
    self.tableView.backgroundColor = CELL_BACKGROUND_2;    
    // show in the status bar that network activity stop
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [self setVideoPlist:nil];
    [self setReleasePlist:nil];    
    
}

- (void)loadVideoPlayer:(NSString *)string
{
    
	
    MPMoviePlayerViewController* theMoviePlayer = [[MPMoviePlayerViewController new] 
                                                   initWithContentURL: [NSURL URLWithString:string]];
    	[self presentMoviePlayerViewControllerAnimated:theMoviePlayer];
    
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [videoPlist count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ApplicationCell";
    
    ApplicationCell *cell = (ApplicationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.useCellBackground = (indexPath.row % 2 == 0);
    
    NSDictionary *videoItem = [videoPlist objectAtIndex:indexPath.row];     
    
    cell.titleLabel.text =[videoItem valueForKey:@"Title"];
    cell.descriptionLabel.text =[videoItem valueForKey:@"Description"];
    cell.pubDateLabel.text =[videoItem valueForKey:@"Date"];
    
    [cell.icon.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [cell.icon.layer setBorderWidth: 1.0];    
    [cell.icon setImageWithURL:[NSURL URLWithString:[videoItem valueForKey:@"ImageUrl"]]
              placeholderImage:[UIImage imageNamed:@"image_thumbnail.png"]];
    
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.backgroundColor = ((ApplicationCell *)cell).useCellBackground ? CELL_BACKGROUND_1 : CELL_BACKGROUND_2;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *videoItem = [videoPlist objectAtIndex:indexPath.row];      
    [self loadVideoPlayer:[videoItem valueForKey:@"VideoUrl"]];         
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection" message:@"You must have an active network connection in order to stream Video" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];    
    }
    
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}



- (IBAction)refresh_:(id)sender {
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self performSelector:@selector(viewDidLoad) withObject:nil];
    [self.tableView reloadData];   
    // show in the status bar that network activity stop
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    
} 
@end
